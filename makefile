# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall

TARGET = readelf

all: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c

clean:
	rm $(TARGET)

test:
	./$(TARGET)

