///
/// @author In Woo Park <inwoo@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   2/24/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include "header.h"

#define bufferSize 1024
typedef enum {F, T} boolean;

/* Initialize struct headers */
Elf32_Ehdr Header32;
Elf64_Ehdr Header64; 

void full (unsigned char buffer[]) {

	for(int i = 0; i < 16; i ++ ){
		printf("%02x ", buffer[i]);
	}
	printf("\n");

	for(int i = 16; i < 32; i ++ ){
		printf("%02x ", buffer[i]);
	}
	printf("\n");

	for(int i = 32; i < 48; i ++ ){
		printf("%02x ", buffer[i]);
	}
	printf("\n");
	
	for(int i = 48; i < 64; i ++ ){
		printf("%02x ", buffer[i]);
	}
	printf("\n");
}

/*
 * This function processes the Magic Header from readelf.
 * It will read the buffer and return the Magic number or the 
 * first row when you call readelf. Those magic numbers have properties 
 * such as 7c 45 4c 46 which spells out _ E L F, or the file type ELF. 
 */ 
void magic (unsigned char buffer[]) {

	/* 
	 * Initialize Variables for error check.
	 * These integers are the integer values of buffer[0...3]. 
	 */
	
	int i;
	unsigned char S = 127;
	unsigned char E = 69;
	unsigned char L = 76; 
	unsigned char F = 70;

	/* Error check to see if the file is an ELF file,
	 * we want to stop immediately if not.
	 */
	if (buffer[0] != S && buffer[1] != E 
			&& buffer[2] != L && buffer[3] != F){
		printf("File is not an ELF file.\n");
		exit(EXIT_FAILURE);
	}

	/* Everything below is the contents for ELF Header we want to see */
	printf("ELF Header:\n");
	printf("  Magic:  ");
	for (i = 0; i < 16; i++){
		printf("%02x ", buffer[i]);
	}
	printf("\n");
}

/*
 * This function processes the Class Header from readelf.
 * It reads the buffer and checks the 4th index to determine 
 * if it is 32 or 64 architecture. 
 */
void class (unsigned char buffer[]) {

	unsigned char architecture64 = 2;
	unsigned char architecture32 = 1;

	if (buffer[4] == architecture64) {
		printf("  Class:				");
		printf("ELF64\n");
		//Address_Size = 1;
	}

	if (buffer[4] == architecture32) {
		printf("  Class:				");
		printf("ELF32\n");
		//Address_Size = 0;
	}
}

/* 
 * Read buffer[5] get info.
 */
void data (unsigned char buffer[]) {

	unsigned char leastSigBit = 1;
	unsigned char mostSigBit = 2;

	if (buffer[5] == leastSigBit) {
		printf("  Data:					");
		printf("%d's commplement, little endian\n", mostSigBit);
	}

	if (buffer[5] == mostSigBit) {
		printf("  Data:					");
		printf("%d's commplement, big endian\n", leastSigBit);
	}
}

/*
 * If buffer[6] is 1, return version 1.
 */
void version (unsigned char buffer[]) {

	//%d, grab version from elf file. 
	unsigned char version = buffer[6]; 

	printf("  Version:				");
	printf("%d (current)\n", version);
}

void OS_ABI (unsigned char buffer[]) {

	const unsigned char ELFOSABI_NONE       = 0   ;    /* UNIX System V ABI */
	const unsigned char ELFOSABI_HPUX       = 1   ;    /* HP-UX */

	if (buffer[7] == ELFOSABI_NONE) {
		printf("  OS/ABI:				");
		printf("UNIX - System V\n");
	}

	if (buffer[7] == ELFOSABI_HPUX) {
		printf("  OS/ABI:				");
		printf("UNIX - HP_UX\n");
	}
}

void ABIVersion (unsigned char buffer[]) {

	unsigned char ABIversion = 0; 

	if (buffer[8] == ABIversion) {
		printf("  ABI Version:				");
		printf("0\n");
	}
}

void type (unsigned char buffer[]) {

	unsigned char REL = 1; 
	unsigned char EXEC = 2; 
	unsigned char DYN = 3; 
	unsigned char CORE = 4; 

	//int x = buffer[17];
	//printf("%d", x);

	if (buffer[16] == REL){
		printf("  Type:					");
		printf("REL (Relocatable file)\n");
	}

	if (buffer[16] == EXEC){
		printf("  Type:					");
		printf("EXEC (Relocatable file)\n");
	}

	if (buffer[16] == DYN){
		printf("  Type:					");
		printf("DYN (Relocatable file)\n");
	}

	if (buffer[16] == CORE){
		printf("  Type:					");
		printf("CORE (Relocatable file)\n");
	}
}

/* 
 * Only 3 machines are implemented because there are too many. 
 */
void machine (unsigned char buffer[]) {

	unsigned char EM_NONE = 0;
	unsigned char EM_M386 = 3;
	unsigned char EM_X86_64 = 62;

	printf("  Machine:				");

	if (buffer[18] == EM_X86_64) {
		printf("Advanced Micro Devices x86-64\n");
	} else if (buffer[18] == EM_M386) {
		printf("Intel 80386\n");
	} else { 
		printf("%c", EM_NONE);
	}
}

void version2 (unsigned char buffer[]) {

	unsigned char version2 = buffer[20];

	printf("  Version:				");
	printf("0x%d \n", version2);
}

/*
 * This function returns the architecture type. 
 */
int findType (unsigned char buffer[]) {
	unsigned char architecture64 = 2;
	unsigned char architecture32 = 1;

	int type64 = 64;
	int type32 = 32;

	if (buffer[4] == architecture64) {
		
		return type64;
	}

	if (buffer[4] == architecture32) {
		
		return type32;
	}

	return 0;
}

/* Starting from this function, I will just call the struct I made */
void entryPointAddress (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Entry point address:			");
		printf("0x %x \n", Header32.e_entry);
	}

	if ((type == 64)) {

		printf("  Entry point address:			");
		printf("0x%lx \n", Header64.e_entry);
		
	}
}


void startOfProgramHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Start of program headers:		");
		printf("%d (bytes into file) \n", Header32.e_phoff);
	}

	if ((type == 64)) {

		printf("  Start of program headers:		");
		printf("%02ld (bytes into file) \n", Header64.e_phoff);
	}
}

void startOfSectionHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Start of section headers:		");
		printf("%d (bytes into file) \n", Header32.e_shoff);
	}

	if ((type == 64)) {

		printf("  Start of section headers:		");
		printf("%ld (bytes into file) \n", Header64.e_shoff);
	}
}

void flags (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Flags:		");
		printf("%d \n", Header32.e_flags);
	}

	if ((type == 64)) {

		printf("  Flags:				");
		printf("0x%d \n", Header64.e_flags);
	}
}

void sizeOfThisHeader (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Size of this headers:			");
		printf("%d \n (bytes)", Header32.e_ehsize);
	}

	if ((type == 64)) {

		printf("  Size of this headers:			");
		printf("%d (bytes)\n", Header64.e_ehsize);
	}
}

void sizeOfProgramHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Size of this program headers:		");
		printf("%d \n (bytes)", Header32.e_phentsize);
	}

	if ((type == 64)) {

		printf("  Size of this program headers:		");
		printf("%d (bytes)\n", Header64.e_phentsize);
	}
}

void numberOfProgramHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Number of program headers:		");
		printf("%d \n ", Header32.e_phnum);
	}

	if ((type == 64)) {

		printf("  Number of program headers:		");
		printf("%d \n", Header64.e_phnum);
	}
}

void sizeOfSectionHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Size of section headers:		");
		printf("%d \n (bytes)", Header32.e_shentsize);
	}

	if ((type == 64)) {

		printf("  Size of section headers:		");
		printf("%d (bytes)\n", Header64.e_shentsize);
	}
}

void numberOfSectionHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Number of section headers:		");
		printf("%d \n ", Header32.e_shnum);
	}

	if ((type == 64)) {

		printf("  Number of section headers:		");
		printf("%d \n", Header64.e_shnum);
	}
}

void sectionHeaderStringTableIndex (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf(" Section header string table index:		");
		printf("%d \n ", Header32.e_shstrndx);
	}

	if ((type == 64)) {

		printf("  Section header string table index:	");
		printf("%d \n", Header64.e_shstrndx);
	}
}


/*
 * This is the main function that will accept for now >./readelf -h [filename]
 * Just like the original readelf, it's purpose is to return (currently) headers. 
 */
int main (int argc, char*argv[]) {

	/* Error check for not enough arguments in argc. "usage" */
	if (argc < 3) {
		fprintf(stderr, "usage: ./readelf [command i.e. -h] [FILE...]\n");
		exit(EXIT_FAILURE);
	}
 
 	/* Initialize variables (boolean, opt, count) */
	boolean do_header = F;
	int opt;
	//Address_Size = ADDRESS_SIZE_UNKNOWN;
	//int count = argc;

	/* 
	 * Loop that uses getopt for command line arguments.
	 * The idea is to use a switch statement to grab additioal 
	 * commands line arguments (i.e. "-h"). 
	 */ 
	while ((opt = getopt(argc, argv, "h")) != -1) {
		switch(opt) {
			case 'h':;
				/* what the heck is happening here idk */
				//i want to process this as a header file 
				do_header = T;
				break;
			case ':':
				printf("option needs a value\n");
				break;
			case '?':
				printf("unknown option: %c\n", optopt);
				break;
		}	
	}

	/* Open the file and create a buffer[] of predefined size */
	FILE * file = fopen(argv[argc - 1], "r");
	unsigned char buffer[bufferSize];

	/* Error check to see if the file opens succesfully */
	if (file == NULL) {
		perror("");
		exit(EXIT_FAILURE);
	}

	/* Read the file and store it into the buffer nicely */ 
	size_t newLen = fread(buffer, sizeof(char), bufferSize, file);
	if (ferror(file) != 0) {
		fputs("Error reading the file", stderr);
		//exit;
	} else {
		buffer[newLen++] = '\0';
	}
	
	/* Because I'm going to fread more than once, I will rewind the file so it goes back
	 * to the top. I will also read it into a struct I made in a file called header.h. 
	 */
	rewind(file);
	fread(&Header32, sizeof(Header32), 1, file);
	rewind(file);
	fread(&Header64, sizeof(Header64), 1, file);
	fclose(file);

	/*
	 * Because the switch statement triggered a boolean 
	 * We are going to start calling the header functions 
	 * to return the contents we want, passing in the buffer 
	 * which is what they are going to read. 
	 */
	if (do_header == T) {
		magic(buffer);
		class(buffer);
		data(buffer);
		version(buffer);
		OS_ABI(buffer);
		ABIVersion(buffer);
		type(buffer);
		machine(buffer);
		version2(buffer);
		entryPointAddress(buffer);
		startOfProgramHeaders(buffer);
		startOfSectionHeaders(buffer);
		flags(buffer);
		sizeOfThisHeader(buffer);
		sizeOfProgramHeaders(buffer);
		numberOfProgramHeaders(buffer);
		sizeOfSectionHeaders(buffer);
		numberOfSectionHeaders(buffer);
		sectionHeaderStringTableIndex(buffer);
	}
}
